


fetch('https://jsonplaceholder.typicode.com/todos', {
   method: 'GET',
   headers: {
   'Content-type': 'application/json',
   }
  
})
.then((response) => response.json())
.then(json => {
   json.map((data) => {
    console.log(data.id, data.title)

  })
});


fetch('https://jsonplaceholder.typicode.com/todos', {
   method: 'GET',
   headers: {
   'Content-type': 'application/json',
   }
  
})
.then((response) => response.json())
.then(json => {
   json.map((data) => {
    console.log(data.title, data.completed)

  })
});

fetch('https://jsonplaceholder.typicode.com/todos', {
   method: 'POST', 
   headers: {
      'Content-type': 'application/json',

   },
   body: JSON.stringify({
     
      title: 'NEW POST',
      completed: true,
      userId: 1
   })
})
.then((response) => response.json())
.then((json) => console.table(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method: 'PUT', 
   headers: {
      'Content-type': 'application/json',

   },
   body: JSON.stringify({
     id: 1,
     title: 'Updated post',
     description: 'Hello again',
     completed: true,
     dateCompleted: '11/30/22',
     userId: 1
   })
})
.then((response) => response.json())
.then((json) => console.table(json));



fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method: 'PATCH', 
   headers: {
      'Content-type': 'application/json',

   },
   body: JSON.stringify({
     id: 1,
     title: 'Patched post',
     description: 'Hello again',
    completed: 'Complete',
     dateChanged: '11/30/22',
     userId: 1
   })
})
.then((response) => response.json())
.then((json) => console.table(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method: 'DELETE'
});

